#[macro_use]
extern crate failure;
use std::io;
use structopt::StructOpt;

mod one;
mod two;

#[derive(Fail, Debug)]
enum MadError {
    #[fail(display = "This is bad: {:?}", name)]
    SickError { name: String },
}

#[derive(StructOpt)]
struct Config {
    day: u8,
    part: String,
}

fn main() {
    let config = Config::from_args();
    let stdin = io::stdin();
    let mut buffer = String::new();

    loop {
        match stdin.read_line(&mut buffer) {
            Ok(n) if n > 0 => (),
            Ok(_) => break,
            Err(_error) => break,
        }
    }

    match config.day {
        1 => match config.part.as_ref() {
            "a" => one::a(buffer).unwrap(),
            "b" => one::b(buffer).unwrap(),
            _x => {
                println!("how come this sucks");
                15
            }
        },
        2 => match config.part.as_ref() {
            "a" => {
                let s = two::a(buffer).unwrap();
                println!("solution: {}", s);
                s
            }
            _x => {
                println!("b doesn't work yet");
                16
            }
        },
        _ => {
            println!("What the barf?");
            10
        }
    };
}
