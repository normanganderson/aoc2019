use std::io::Error;

pub fn a(buffer: String) -> Result<u64, Error> {
    let mut modules: Vec<u64> = vec![];
    for line in buffer.lines() {
        match line.parse::<u64>() {
            Ok(parsed) => modules.push(parsed),
            Err(err) => println!("Error {}", err),
        }
    }

    let mut price_sum: u64 = 0;
    modules.iter().for_each(|module| {
        let price = (module / 3) - 2;
        price_sum += price;
    });

    println!("price for all modules is {}", price_sum);
    Ok(price_sum)
}

pub fn b(buffer: String) -> Result<u64, Error> {
    let mut modules: Vec<u64> = vec![];
    for line in buffer.lines() {
        match line.parse::<u64>() {
            Ok(parsed) => modules.push(parsed),
            Err(err) => println!("Error {}", err),
        }
    }

    let mut fuel_sum: u64 = 0;
    modules.iter().for_each(|module| {
        let fuel = (module / 3) - 2;
        let fuel_fuel = fuel_fuel(fuel);
        println!("fuel for module is {}", fuel);
        println!("fuel for fuel is {}", fuel_fuel);
        fuel_sum += fuel + fuel_fuel;
    });

    println!("fuel for all modules is {}", fuel_sum);
    Ok(fuel_sum)
}

fn fuel_fuel(module_fuel: u64) -> u64 {
    let mut fuel_fuel: u64 = 0;
    let mut last_fuel: u64 = module_fuel;
    loop {
        let this_ff = last_fuel / 3;
        if this_ff > 2 {
            println!("fuel for fuel {} is {}", last_fuel, this_ff);
            last_fuel = this_ff - 2;
            fuel_fuel += this_ff - 2;
        } else {
            println!("fuel for {} is negative or 0 {}", last_fuel, this_ff);
            break;
        }
    }
    fuel_fuel
}
