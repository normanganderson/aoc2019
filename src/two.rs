use crate::MadError;
use failure::Error;

pub fn a(buffer: String) -> Result<u64, Error> {
    let mut ops: Vec<u64> = Vec::new();
    for x in buffer.split(",") {
        match x.trim().parse::<u64>() {
            Ok(parsed) => ops.push(parsed),
            Err(err) => println!("**Error {} for {}**", err, x),
        }
    }

    let mut op_idx: usize = 0;
    loop {
        let output_idx = ops[op_idx + 3];
        let x = match ops[op_idx] {
            1 => Ok(ops[ops[op_idx + 2] as usize] + ops[ops[op_idx + 1] as usize]),
            2 => Ok(ops[ops[op_idx + 2] as usize] * ops[ops[op_idx + 1] as usize]),
            99 => Err(MadError::SickError {
                name: "done".to_string(),
            }),
            x => Err(MadError::SickError {
                name: format!("{}", x),
            }),
        };
        match x {
            Ok(result) => ops[output_idx as usize] = result,
            Err(_) => break,
        };
        op_idx += 4;
    }

    Ok(*ops.first().unwrap())
}
